<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MountainSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mountain-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Id_location') ?>

    <?= $form->field($model, 'Nama_Gunung') ?>

    <?= $form->field($model, 'Lokasi_Gunung') ?>

    <?= $form->field($model, 'DPL') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
